const sequelize = require('../database/database');
const { DataTypes, Model } = require('sequelize');

const Solucion = sequelize.define('SOLUCION', {
    // Model attributes are defined here

    id_error: {
        type: DataTypes.INTEGER
        // allowNull defaults to true
    },
    frecuencia: {
        type: DataTypes.INTEGER
        // allowNull defaults to true
    },
    name: {
        type: DataTypes.STRING
    },
    estado_solucion: {
        type: DataTypes.STRING
    }
}, {
    sequelize, timestamps: false, tableName: 'SOLUCION'
}, {
    // Other model options go here
});
module.exports = Solucion