const sequelize = require('../database/database');
const { DataTypes, Model } = require('sequelize');

const Error = sequelize.define('ERROR', {
    // Model attributes are defined here

    id_subdependencia: {
        type: DataTypes.INTEGER
        // allowNull defaults to true
    },
    frecuencia: {
        type: DataTypes.INTEGER
        // allowNull defaults to true
    },
    name: {
        type: DataTypes.STRING
    },
    estado_error: {
        type: DataTypes.STRING
    }
}, {
    sequelize, timestamps: false, tableName: 'ERROR'
}, {
    // Other model options go here
});
module.exports = Error