const sequelize = require('../database/database');
const { DataTypes, Model } = require('sequelize');

const Dependencia = sequelize.define('DEPENDENCIA', {
    // Model attributes are defined here

    id_area: {
        type: DataTypes.INTEGER
        // allowNull defaults to true
    },
    name: {
        type: DataTypes.STRING
    },
    estado_dependencia: {
        type: DataTypes.STRING
    }
}, {
    sequelize, timestamps: false, tableName: 'DEPENDENCIA'
}, {
    // Other model options go here
});
module.exports = Dependencia



