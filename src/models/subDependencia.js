const sequelize = require('../database/database');
const { DataTypes, Model } = require('sequelize');

const SubDependencia = sequelize.define('SUB_DEPENDENCIA', {
    // Model attributes are defined here

    id_dependencia: {
        type: DataTypes.INTEGER
        // allowNull defaults to true
    },
    name: {
        type: DataTypes.STRING
    },
    estado_sub_dependencia: {
        type: DataTypes.STRING
    }
}, {
    sequelize, timestamps: false, tableName: 'SUB_DEPENDENCIA'
}, {
    // Other model options go here
});
module.exports = SubDependencia



