const sequelize = require('../database/database');
const { DataTypes, Model } = require('sequelize');

const Area = sequelize.define('AREAS', {
    // Model attributes are defined here

    name: {
        type: DataTypes.STRING
        // allowNull defaults to true
    },
    estado_area: {
        type: DataTypes.STRING
    }
}, {
    sequelize, timestamps: false, tableName: 'AREAS'
}, {
    // Other model options go here
});











/*
ESTO FUNCIONÓ 

class AREAS extends Model { }
AREAS.init({

    NAME: {
        type: DataTypes.STRING
        // allowNull defaults to true
    },
    ESTADO_AREA: {
        type: DataTypes.STRING
    },

}, { sequelize, timestamps: false, tableName: 'AREAS' });
*/

// // // the defined model is the class itself
// // console.log(Area === sequelize.models.AREA); // true

module.exports = Area

