const dataSettings = require('../models/settings.json');
let controller = {};

controller.getSettings = (req, res) => {
    res.status(200).json(dataSettings)
}

controller.setImgProfile = (req, res) => {
    const { params: { imgProfile } } = req;
    dataSettings.imgProfile = !dataSettings.imgProfile
    res.status(200).json({
        ok: true,
        data: dataSettings
    })

}

controller.setChat = (req, res) => {
    const { params: { chat } } = req;
    dataSettings.chat = !dataSettings.chat

    res.status(200).json({
        ok: 'chat',
        data: dataSettings
    })
}

controller.setTheme = (req, res) => {
    const { params: { theme } } = req;
    dataSettings.theme = theme
    res.status(200).json({
        ok: 'theme',
        data: dataSettings
    })
}



module.exports = controller;