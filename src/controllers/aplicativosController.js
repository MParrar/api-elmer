const dataApps = require('../models/apps.json');

let controller = {};

controller.getApps = (req, res) => {
    return res.status(200).json(dataApps);
}

module.exports = controller;