const Area = require('../models/area');
let controller = {};
/**
 * 
 * @param {*} req 
 * @param {*} res 
 * Este metodo retorna desde la bdd las areas existentes
 */
controller.getAreas = async (req, res) => {
    const areas = await Area.findAll();
    res.status(200).json(areas);
}

module.exports = controller;