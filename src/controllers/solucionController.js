const dataSolucion = require('../models/Soluciones.json');
const dataError = require('../models/Errores.json');
let dataContadorDeErrores = require('../models/contadorDeErrores.json')
const Solucion = require('../models/solucion');

let controller = {};
/**
 * 
 * @param {id_error} req 
 * @param {*} res 
 * Este metodo devuelve las soluciones asociadas al error seleccionado
 */
controller.filterSolucion = async (req, res) => {
    const { params: { id_error } } = req;
    const solucion = await Solucion.findAll({ where: { id_error } });
    console.log(solucion)
    res.status(200).json({
        msg: "Estas son las posibles soluciones a tu problema",
        options: solucion,
        transmitter: 'b',
        next: 'resultado'
    });
}


const getSolucion = (id) => {
    return dataSolucion.filter((item) => item.Id_Error === id)
}



module.exports = controller;
