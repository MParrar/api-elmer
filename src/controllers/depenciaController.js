const Dependencia = require('../models/dependencia');

let controller = {};
/**
 * 
 * @param {*} req 
 * @param {*} res 
 * Este metodo devuelve las dependencias asociadas al id del area seleccionada
 */
controller.filterDependencia = async (req, res) => {
    const { params: { id_area } } = req;
    const dependencia = await Dependencia.findAll({ where: { id_area } });
    res.status(200).json({
        msg: "¿En qué aplicativo tienes problemas?",
        options: dependencia,
        transmitter: 'b',
        next: 'subDependencia'
    });
}

module.exports = controller;