const SubDependencia = require('../models/subDependencia');

let controller = {};
/**
 * 
 * @param {*} req 
 * @param {*} res 
 * Este metodo retorna las subDependencia asociadas a la dependencia seleccionada
 */
controller.filterSubDependencia = async (req, res) => {
    const { params: { id_dependencia } } = req;
    const subDependencia = await SubDependencia.findAll({ where: { id_dependencia } });
    console.log(subDependencia)
    res.status(200).json({
        msg: "¿Con qué funcionalidad tienes problemas?",
        options: subDependencia,
        transmitter: 'b',
        next: 'error'
    });
}

module.exports = controller;