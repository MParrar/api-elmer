const moment = require('moment');

let controller = {};

let now = moment().format('A');
let adios = {
    msg: ''
}
let data = [{
    id: 1,
    name: 'Si, mi problema se solucionó'
}, {
    id: 2,
    name: 'No, mi problema no se solucionó'
}]


controller.respuestaSolucion = (req, res) => {
    return res.status(200).json({
        msg: "¿Se solucionó su problema?",
        options: data,
        transmitter: 'b',
        next: 'resultado/respuesta',

    })
}

controller.respuestaUsuario = (req, res) => {
    const { params: { id } } = req;
    if (parseInt(id) === 1) {
        return res.status(200).json({
            msg: mensajeAdios(now.toString()),
            transmitter: 'b'

        })
    } else {
        return res.status(200).json({
            msg: 'Por favor comunicarse con Development@falabella.cl',
            transmitter: 'b'
        })
    }
}


let mensajeAdios = (hora) => {
    if (hora === 'PM') {
        return adios.msg = "Buenas tardes, fue un placer ayudarte"
    } else {
        return adios.msg = "Buenos Dias, fue un placer ayudarte"
    }
}

module.exports = controller;