const Error = require('../models/error');

let controller = {};
/**
 * 
 * @param {*} req 
 * @param {*} res 
 * Este metodo devuelve los errores asociados a la subdepencia
 */
controller.filterError = async (req, res) => {
    const { params: { id_subdependencia } } = req;
    const error = await Error.findAll({ where: { id_subdependencia } });
    console.log(error)
    res.status(200).json({
        msg: "¿Cuál es el problema?",
        options: error,
        transmitter: 'b',
        next: 'solucion'
    });
}

module.exports = controller;