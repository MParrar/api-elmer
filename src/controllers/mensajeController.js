const moment = require('moment');
const dataArea = require('./../models/Area.json');

let now = moment().format('A');
let controller = {};


controller.getMensajes = (req, res) => {
    return res.status(200).json({
        msg: mensajeBienvenida(now.toString()),
        options: dataArea,
        transmitter: 'b',
        next: 'dependencia'
    })
}

controller.getMensajeDespedida = (req, res) => {
    return res.status(200).json({
        msg: mensajeAdios(now.toString()),
        transmitter: 'b'
    })
}

const mensajeBienvenida = (hora) => {
    if (hora === 'PM') {
        return "Buenas tardes, ¿Cuál es tu area?"
    } else {
        return "Buenos Dias, ¿Cuál es tu area?"
    }
}

const mensajeAdios = (hora) => {
    if (hora === 'PM') {
        return "Buenas tardes, fue un placer ayudarte"
    } else {
        return "Buenos Dias, fue un placer ayudarte"
    }
}

module.exports = controller;