const express = require('express');
const app = express();

app.use('/area', require('./areaRoute'));
app.use('/mensaje', require('./mensajeRoute'));
app.use('/dependencia', require('./dependenciaRoute'));
app.use('/subDependencia', require('./subDependenciaRoute'));
app.use('/error', require('./errorRoute'));
app.use('/solucion', require('./solucionRoute'));
app.use('/aplicativos', require('./aplicativosRoute'));
app.use('/resultado', require('./resultadoRouter'));
app.use('/settings', require('./settingsRoute'));

module.exports = app;
