const express = require('express');
const router = express.Router();
const middleware = require('../middleware/veryfiToken');
const { getAreas } = require('../controllers/areaController');


router.get('/', middleware, getAreas);


module.exports = router;