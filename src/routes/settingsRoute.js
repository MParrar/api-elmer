const express = require('express');
const router = express.Router();
const middleware = require('../middleware/veryfiToken');
const { getSettings, setImgProfile, setTheme, setChat } = require('../controllers/settingsController');


router.get('/', getSettings);
router.get('/img/:imgProfile', setImgProfile);
router.get('/theme/:theme', setTheme);
router.get('/chat/:chat', setChat);


module.exports = router;