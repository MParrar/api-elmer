const express = require('express');
const { filterError } = require('../controllers/errorController');
const router = express.Router();
const middleware = require('../middleware/veryfiToken');


router.get('/:id_subdependencia', middleware, filterError);

module.exports = router;