const express = require('express');
const router = express.Router();
const middleware = require('../middleware/veryfiToken');
const { filterSolucion } = require('../controllers/solucionController');

router.get('/:id_error', middleware, filterSolucion);

module.exports = router;