const express = require('express');
const { filterDependencia, filterDependencia2 } = require('../controllers/depenciaController');
const router = express.Router();
const middleware = require('../middleware/veryfiToken');

router.get('/:id_area', middleware, filterDependencia,);


module.exports = router;