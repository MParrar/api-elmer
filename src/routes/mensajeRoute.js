const express = require('express');
const router = express.Router();
const middleware = require('../middleware/veryfiToken');
const { getMensajes, getMensajeDespedida } = require('../controllers/mensajeController');

router.get('/', middleware, getMensajes);
router.get('/despedida/:id', middleware, getMensajeDespedida);
module.exports = router;