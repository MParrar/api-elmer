const express = require('express');
const router = express.Router();
const middleware = require('../middleware/veryfiToken');
const { getApps } = require('../controllers/aplicativosController');

router.get('/', middleware, getApps);

module.exports = router;