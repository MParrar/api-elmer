const express = require('express');
const { filterSubDependencia } = require('../controllers/subDependenciaController');
const router = express.Router();
const middleware = require('../middleware/veryfiToken');

router.get('/:id_dependencia', middleware, filterSubDependencia);

module.exports = router;