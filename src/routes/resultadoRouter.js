const express = require('express');
const router = express.Router();
const middleware = require('../middleware/veryfiToken');
const { respuestaSolucion, respuestaUsuario } = require('../controllers/resultadoController');

router.get('/:id', middleware, respuestaSolucion);
router.get('/respuesta/:id', middleware, respuestaUsuario)

module.exports = router;