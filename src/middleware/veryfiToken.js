function verifyToken(req, res, next) {
    const token = req.headers['api-key'];
    if (token == process.env.APIKEY) {
        next();
    } else {
        return res.status(401).json({
            auth: false,
            message: 'No Valid Token'
        })
    }
}

module.exports = verifyToken;