const { Sequelize } = require('sequelize');


const sequelize = new Sequelize('LF-ELMER-TEST', 'elmer', 'elmer', {
    host: '10.38.60.6',
    dialect: 'mssql',

    dialectOptions: {
        options: {
            encrypt: false
        }
    }
});



module.exports = sequelize;