const express = require('express');
const app = express();
const sequelize = require('./database/database');

const cors = require('cors');
require('dotenv').config();

// Base de datos


//middlewares
app.use(cors());
app.use(express.json())

// routes
app.use('/api', require('./routes'));

function initServer() {
    app.listen(process.env.PORT)
    console.log(`Server on port: ${process.env.PORT}`)
    sequelize.sync({ force: false }).then(() => {
        console.log("EN LA BASE DE DATOS")
    }).catch(err => {
        console.log(err)
    })
}

initServer();